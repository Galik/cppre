# cppre (C++ Regular Expressions) - Command Line Tool
Command line regex tool based on `C++11` standard regex.

This tool is intended to be useful on the command line for script processing.
It uses the `C++` standard regular expression engine.

**Status:** Alpha

## Installation (to /usr/local - requires root privilege (sudo))

```bash
$ ./bootstrap.sh
$ ./configure-system-release.sh
$ cd build-system
$ make
$ sudo make install
$ make clean
```

### usage:
```bash
# mode = +m|+s|+i|+t
cppre +<mode> [options] "text" "regex"  # match text
cppre +<mode> [options] -f file "regex" # match file
cppre +<mode> [options] "regex"         # match stdin
   
# mode = +r adds an additional parameter
cppre +r [options] "text" "regex" "replacement" # replace matched text
```
### Modes:
 ```bash   
+m match (std::regex_match)               # Whole text must match
+s search (std::regex_search)             # Part of text must match
+r replace (std::regex_replace)           # Output replaces matched text
+i iterate (std::wcregex_iterator)        # Output multiple matches
+t tokenize (std::wcregex_token_iterator) # Output multiple matches
```    
### Options:
```bash
# set regex engine
-ECMA (default)
-basic 
-extended 
-awk 
-grep 
-egrep 
-icase 
-nosubs 
-collate 
    
-l|--line # line mode (one line at a time)

-q  # Quiet - no output, just set the return status

-gn # n = 0-9, g0 to g9 output the capture group
    # associated with the number.
    
-ld|--line-delim  "delimiter" # string to separate output lines by (when using --line).
-gd|--group-delim "delimiter" # string to separate output groups by.
-md|--match-delim "delimiter" # string to separate output matches by.
    
# preset delimiters

      # These are used when running in *line mode* (--line)
-ldnl # separate lines with a new-line character [default]
-ldws # separate lines with a space character
-ldno # do not separate lines
    
-mdnl # separate matches with a new-line character [default]
-mdws # separate matches with a space character
-mdno # do not separate matches
    
-gdnl # separate groups with a new-line character
-gdws # separate groups with a space character
-gdno # do not separate groups [default]
```
### Examples:
```bash
$ cppre +m -g2 -g1 "abcde" "(...)(..)"
> deabc    

$ cppre +m -gd " " -g2 -g1 "abcde" "(...)(..)"
> de abc
    
$ cppre +m -gdws -g2 -g1 -g3 "abc def ghi" "(\w+)\s+(\w+)\s+(\w+)"    
> def abc ghi
    
$ cppre +m -gdnl -g2 -g1 -g3 "abc def ghi" "(\w+)\s+(\w+)\s+(\w+)"    
> def
> abc
> ghi
```    
    

