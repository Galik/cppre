//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "options.hpp"
#include "regex.hpp"

NAMESPACE_CPPRE_BEGIN;

int search(std::ostream& os, const char* const* arg)
{
	auto opts = parse_options(arg);

	if(!arg[0])
		return usage_search(os, usage_mode::specific_help, EXIT_FAILURE);

	regex_params params = get_params(opts, arg);

	bool res = false;

	auto l_sep = "";
	for(auto const& line: params.lines)
	{
		std::wsmatch m;
		if(!std::regex_search(line, m, params.e, opts.match_flags))
			continue;

		res = true;

		if(!opts.quiet)
		{
			auto m_sep = "";

			os << l_sep;
			l_sep = opts.line_delim;

			os << m_sep;
			m_sep = opts.match_delim;

			auto g_sep = "";
			for(unsigned g: opts.groups)
			{
				if(g < m.size())
				{
					os << g_sep << to_utf8(m.str(g));
					g_sep = opts.group_delim;
				}
			}
		}
	}

	return !res;
}

NAMESPACE_CPPRE_END;
