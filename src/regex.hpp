#ifndef CPPRE_REGEX_H
#define CPPRE_REGEX_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//


#include <regex>
#include <string>
#include <vector>
#include <cassert>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <functional>

#include <experimental/filesystem>

#include "defs.hpp"
#include "options.hpp"


NAMESPACE_CPPRE_BEGIN;

class separator
{
	const char* c;
	const char* s;

public:
	separator(const char* s): s(s) {}
	void reset() { c = ""; }
	void reset(const char* s) { c = ""; this->s = s; }
	friend std::string operator+(std::string s, separator& sep)
	{
		s.append(sep.c);
		sep.c = sep.s;
		return s;
	}
	friend std::string operator+(separator& sep, std::string s)
	{
		s.insert(0, sep.c);
		sep.c = sep.s;
		return s;
	}
};

struct regex_params
{
	std::vector<std::wstring> lines;
	std::wregex e;
};

std::string load_file(fs::path filepath);

regex_params get_params(const options& opts, const char* const* arg);
int match(std::ostream& os, const char* const* arg);
int search(std::ostream& os, const char* const* arg);
int iterate(std::ostream& os, const char* const* arg);
int replace(std::ostream& os, const char* const* arg);
int tokenize(std::ostream& os, const char* const* arg);

int usage_match(std::ostream& os, usage_mode mode, int error);
int usage_search(std::ostream& os, usage_mode mode, int error);
int usage_iterate(std::ostream& os, usage_mode mode, int error);
int usage_replace(std::ostream& os, usage_mode mode, int error);
int usage_tokenize(std::ostream& os, usage_mode mode, int error);

inline
std::wstring from_utf8(std::string const& s)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cnv;
	std::wstring to = cnv.from_bytes(s);
	if(cnv.converted() < s.size())
		throw std::runtime_error("incomplete conversion");
	return to;
}

inline
std::string to_utf8(std::wstring const& s)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cnv;
	std::string to = cnv.to_bytes(s);
	if(cnv.converted() < s.size())
		throw std::runtime_error("incomplete conversion");
	return to;
}

inline std::vector<std::wstring> load_data(const options& opts, const std::string& utf8)
{
	bug_fun();
	bug_var("'" << utf8 << "'");

	std::wstring s = from_utf8(utf8);

	std::vector<std::wstring> lines;

	if(!opts.line_by_line)
		lines.push_back(s);
	else
	{
		std::wstring line;
		std::wistringstream iss(s);
		while(std::getline(iss, line))
			lines.push_back(line);
	}

	return lines;
}

int regex(std::ostream& os, const char* const argv[]);

NAMESPACE_CPPRE_END;

#endif // CPPRE_REGEX_H
