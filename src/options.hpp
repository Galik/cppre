#ifndef CPPRE_OPTIONS_H
#define CPPRE_OPTIONS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <regex>
#include <string>
#include <vector>
#include <cstdint>

#include "defs.hpp"

NAMESPACE_CPPRE_BEGIN;

enum class usage_mode {main_help, specific_help, create_docs};

int usage(std::ostream& os, const fs::path& prog, usage_mode mode, int error);
int version(std::ostream& os, const fs::path& prog, int error);

struct options
{
	using match_flag_type = std::regex_constants::match_flag_type;
	using syntax_option_type = std::regex_constants::syntax_option_type;

	syntax_option_type syntax = std::regex_constants::ECMAScript;
	syntax_option_type syntax_flags = {};
	match_flag_type match_flags = std::regex_constants::match_default;
	bool quiet = false; // print matching string
	bool line_by_line = false;
	std::vector<int> groups; // groups to print in given order
	const char* line_delim = "\n";
	const char* match_delim = "\n";
	const char* group_delim = "";
	const char* group_format = nullptr;
	const char* input_file = nullptr;
//	const char* output_file = nullptr;
	enum class engine_type { std, pcre };
	engine_type engine = engine_type::std;
};

options parse_options(const char* const*& arg);

NAMESPACE_CPPRE_END;

#endif // CPPRE_OPTIONS_H
