//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <regex>
#include <string>
#include <cstring>
#include <iterator>
#include <iostream>

#include "regex.hpp"

#include "options.hpp"

NAMESPACE_CPPRE_BEGIN;

std::string decode(std::regex_constants::error_type e)
{
	using namespace std::regex_constants;

	if(e == error_collate) return "invalid collating element name";
	else if(e == error_ctype) return "invalid character class name";
	else if(e == error_escape) return "invalid escaped character or a trailing escape";
	else if(e == error_backref) return "invalid back reference";
	else if(e == error_brack) return "mismatched square brackets ('[' and ']')";
	else if(e == error_paren) return "mismatched parentheses ('(' and ')')";
	else if(e == error_brace) return "mismatched curly braces ('{' and '}')";
	else if(e == error_badbrace) return "invalid range in a {} expression";
	else if(e == error_range) return "invalid character range (e.g. [b-a])";
	else if(e == error_space) return "out of memory";
	else if(e == error_badrepeat) return "one of *?+{ was not preceded by a valid regular expression";
	else if(e == error_complexity) return "match is too complex";
	else if(e == error_stack) return "not enough memory to perform a match";
	return "unknown error";
}

std::string load_file(fs::path filepath)
{
	if(!fs::exists(filepath))
		throw_runtime_error("No such file: " << filepath);

	if(!fs::is_regular_file(filepath))
		throw_runtime_error("Not a regular file file: " << filepath);

	std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);

	if(!ifs)
		throw errno_error();

	std::string file(ifs.tellg(), '\0');

	if(!ifs.seekg(0).read(&file[0], file.size()))
		throw errno_error();

	return file;
}

regex_params get_params(const options& opts, const char* const* arg)
{
	assert(arg[0]);

	regex_params params;

	auto syntax = opts.syntax|opts.syntax_flags|std::regex_constants::optimize;

	if(arg[1])
	{
		if(opts.input_file)
			throw_runtime_error("Cannot combine input file (-i) with input string");

//		if(have_piped_data())
//			throw_runtime_error("Cannot combine input string with piped input");

		return {load_data(opts, arg[0]), std::wregex{from_utf8(arg[1]), syntax}};
	}
	else if(opts.input_file)
	{
//		if(have_piped_data())
//			throw_runtime_error("Cannot combine input file (-i) with piped input");

		return {load_data(opts, load_file(opts.input_file)), std::wregex{from_utf8(arg[0]), syntax}};
	}

	// assume piped data

//	if(!have_piped_data())
//		throw_runtime_error("Need either input string, input file (-i) or piped input");

	std::ostringstream oss;
	oss << std::cin.rdbuf();
	std::string s = oss.str();
	if(!s.empty())
		s.erase(s.end() - 1); // remove trailing newline
	return {load_data(opts, s), std::wregex{from_utf8(arg[0]), syntax}};
}

int regex(std::ostream& os, const char* const argv[])
{
	try
	{
		if(!argv[1])
			return usage(os, argv[0], usage_mode::main_help, EXIT_FAILURE);

		if(!std::strcmp(argv[1], "-h")
		|| !std::strcmp(argv[1], "--help"))
			return usage(os, argv[0], usage_mode::main_help, EXIT_SUCCESS);
		else if(!std::strcmp(argv[1], "--version"))
			return version(os, argv[0], EXIT_SUCCESS);
		else if(!std::strcmp(argv[1], "+m")
			 || !std::strcmp(argv[1], "match"))
			return match(os, argv + 2);
		else if(!std::strcmp(argv[1], "+s")
			 || !std::strcmp(argv[1], "search"))
			return search(os, argv + 2);
		else if(!std::strcmp(argv[1], "+i")
			 || !std::strcmp(argv[1], "iterate"))
			return iterate(os, argv + 2);
		else if(!std::strcmp(argv[1], "+r")
			 || !std::strcmp(argv[1], "replace"))
			return replace(os, argv + 2);
		else if(!std::strcmp(argv[1], "+t")
			 || !std::strcmp(argv[1], "tokenize"))
			return tokenize(os, argv + 2);
	}
	catch(const std::regex_error& e)
	{
		std::cerr << decode(e.code()) << '\n';
		return EXIT_FAILURE;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return EXIT_FAILURE;
	}
	catch(...)
	{
		std::cerr << "Unknown error" << '\n';
		return EXIT_FAILURE;
	}

	return usage(os, argv[0], usage_mode::main_help, EXIT_FAILURE);
}

NAMESPACE_CPPRE_END;

