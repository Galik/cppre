//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "options.hpp"
#include "regex.hpp"

NAMESPACE_CPPRE_BEGIN;

int replace(std::ostream& os, const char* const* arg)
{
	auto opts = parse_options(arg);

	if(!arg[0] || !arg[1])
		return usage_replace(os, usage_mode::specific_help, EXIT_FAILURE);

	std::vector<std::wstring> lines; // input
	std::wstring f; // format
	std::wregex e;

	// "text" "regex" "fmt" // arg[0] arg[1] arg[2]
	// -f file.txt "regex" "fmt" // opts.input_file arg[0] arg[1]
	// "regex" "fmt" // std::cin  arg[0] arg[1]

	if(arg[2])
	{
		lines = load_data(opts, arg[0]);
		e = std::wregex(from_utf8(arg[1]), opts.syntax|opts.syntax_flags);
		f = from_utf8(arg[2]);
	}
	else if(opts.input_file)
	{
		lines = load_data(opts , load_file(opts.input_file));
		e = std::wregex(from_utf8(arg[0]), opts.syntax|opts.syntax_flags);
		f = from_utf8(arg[1]);
	}
	else
	{
		std::ostringstream oss;
		oss << std::cin.rdbuf();
		lines = load_data(opts, oss.str());
		e = std::wregex(from_utf8(arg[0]), opts.syntax|opts.syntax_flags);
		f = from_utf8(arg[1]);
	}

	auto l_sep = "";
	const std::wsregex_iterator end;

	for(auto const& s: lines)
	{
		std::wsregex_iterator itr(s.begin(), s.end(), e, opts.match_flags);

		os << l_sep;
		l_sep = opts.line_delim;

		if(itr == end)
			os << to_utf8(s);
		else
		{
			bool first = true;
			std::wstring ws;
			std::wsregex_iterator last_itr;

			for(; itr != end; ++itr)
			{
				if(!(opts.match_flags & std::regex_constants::format_no_copy))
				{
					ws.assign(itr->prefix().first, itr->prefix().second);
					os << to_utf8(ws);
				}

				if(!(opts.match_flags & std::regex_constants::format_first_only)
				|| !first)
				{
					ws.clear();
					itr->format(std::back_inserter(ws), f, opts.match_flags);
					os << to_utf8(ws);
					first = false;
				}

				last_itr = itr;
			}

			if(!(opts.match_flags & std::regex_constants::format_no_copy))
			{
				ws.assign(last_itr->suffix().first, last_itr->suffix().second);
				os << to_utf8(ws);
			}
		}
	}

	return EXIT_SUCCESS;
}

NAMESPACE_CPPRE_END;
