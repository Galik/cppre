//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <regex>
#include <string>
#include <cstring>
#include <iterator>
#include <iostream>

#include "regex.hpp"

NAMESPACE_CPPRE_BEGIN;

int version(std::ostream& os, const fs::path& prog, int error)
{
	os << prog.filename().string() << " " << VERSION << '\n';
	return error;
}

int usage(std::ostream& os, const fs::path& prog, usage_mode mode, int error)
{
	os << "" << '\n';
	os << "USAGE" << '\n';
	os << "" << '\n';
	os << prog.filename().string() << " (match|search|iterate|replace) [OPTIONS] ..." << '\n';
	os << prog.filename().string() << " (+m|+s|+i|+r) [OPTIONS] ..." << '\n';
	os << "" << '\n';
	os << "OPTIONS (syntax)" << '\n';
	os << "" << '\n';
	os << " -ECMA (default)" << '\n';
	os << " -basic " << '\n';
	os << " -extended " << '\n';
	os << " -awk " << '\n';
	os << " -grep " << '\n';
	os << " -egrep " << '\n';
	os << " -icase " << '\n';
	os << " -nosubs " << '\n';
	os << " -collate " << '\n';
	os << "" << '\n';
	os << "OPTIONS (input)" << '\n';
	os << "" << '\n';
	os << "-l|--line           - line mode (one line at a time)" << '\n';
	os << "" << '\n';
	os << "OPTIONS (output)" << '\n';
	os << "" << '\n';
	os << "-q                  - quiet, no output, just sets the return state" << '\n';
	os << "-gn (where n = 0-9) - output capture group n" << '\n';
	os << "" << '\n';
	os << "-ld|--line-delim    - \"delimiter\" string to separate output lines (with --line)." << '\n';
	os << "-gd|--group-delim   - \"delimiter\" string to separate output groups." << '\n';
	os << "-md|--match-delim   - \"delimiter\" string to separate output matches." << '\n';
	os << "" << '\n';
	os << "OPTIONS (preset delimiters)" << '\n';
	os << "" << '\n';
	os << "                    - These are used when running in *line mode* (--line)" << '\n';
	os << "-ldnl               - separate lines with a new-line character [default]" << '\n';
	os << "-ldws               - separate lines with a space character" << '\n';
	os << "-ldno               - do not separate lines" << '\n';
	os << "" << '\n';
	os << "-mdnl               - separate matches with a new-line character [default]" << '\n';
	os << "-mdws               - separate matches with a space character" << '\n';
	os << "-mdno               - do not separate matches" << '\n';
	os << "" << '\n';
	os << "-gdnl               - separate groups with a new-line character" << '\n';
	os << "-gdws               - separate groups with a space character" << '\n';
	os << "-gdno               - do not separate groups [default]" << '\n';
	os << "" << '\n';
	usage_match(os, mode, error);
	os << "" << '\n';
	usage_search(os, mode, error);
	os << "" << '\n';
	usage_iterate(os, mode, error);
	os << "" << '\n';
	usage_replace(os, mode, error);
	os << "" << '\n';
	usage_tokenize(os, mode, error);
	os << "" << '\n';

	return error;
}

options parse_options(const char* const*& arg)
{
	options opts;

	bool debug = false;

	for(; *arg && arg[0][0] == '-'; ++arg)
	{
		if(!std::strcmp(*arg, "--debug"))
			debug = true;
		else if(!std::strcmp(*arg, "-ECMA"))
			opts.syntax = std::regex_constants::ECMAScript;
		else if(!std::strcmp(*arg, "-basic"))
			opts.syntax = std::regex_constants::basic;
		else if(!std::strcmp(*arg, "-extended"))
			opts.syntax = std::regex_constants::extended;
		else if(!std::strcmp(*arg, "-awk"))
			opts.syntax = std::regex_constants::awk;
		else if(!std::strcmp(*arg, "-grep"))
			opts.syntax = std::regex_constants::grep;
		else if(!std::strcmp(*arg, "-egrep"))
			opts.syntax = std::regex_constants::egrep;
		else if(!std::strcmp(*arg, "-icase"))
			opts.syntax_flags |= std::regex_constants::icase;
		else if(!std::strcmp(*arg, "-nosubs"))
			opts.syntax_flags |= std::regex_constants::nosubs;
		else if(!std::strcmp(*arg, "-collate"))
			opts.syntax_flags |= std::regex_constants::collate;
		else if(!std::strcmp(*arg, "-nc")
			 || !std::strcmp(*arg, "--format-no-copy"))
			opts.match_flags |= std::regex_constants::format_no_copy;
		else if(!std::strcmp(*arg, "-q"))
			opts.quiet = true;
		else if(arg[0][1] == 'g' && std::isdigit(arg[0][2]))
			opts.groups.push_back(std::atoi(arg[0] + 2));
		else if(!std::strcmp(*arg, "-ldnl")
			 || !std::strcmp(*arg, "--line-delim-line"))
			opts.match_delim = "\n";
		else if(!std::strcmp(*arg, "-ldws")
			 || !std::strcmp(*arg, "--line-delim-space"))
			opts.match_delim = " ";
		else if(!std::strcmp(*arg, "-ldno")
			 || !std::strcmp(*arg, "--line-delim-none"))
			opts.match_delim = "";
		else if(!std::strcmp(*arg, "-ld")
			 || !std::strcmp(*arg, "--line-delim"))
		{
			if(!(*(++arg)))
				throw_runtime_error((*(arg - 1)) << " expects delimiter string argument");
			opts.line_delim = *arg;
		}
		else if(!std::strcmp(*arg, "-mdnl")
			 || !std::strcmp(*arg, "--match-delim-line"))
			opts.match_delim = "\n";
		else if(!std::strcmp(*arg, "-mdws")
			 || !std::strcmp(*arg, "--match-delim-space"))
			opts.match_delim = " ";
		else if(!std::strcmp(*arg, "-mdno")
			 || !std::strcmp(*arg, "--match-delim-none"))
			opts.match_delim = "";
		else if(!std::strcmp(*arg, "-md")
			 || !std::strcmp(*arg, "--match-delim"))
		{
			if(!(*(++arg)))
				throw_runtime_error((*(arg - 1)) << " expects delimiter string argument");
			opts.match_delim = *arg;
		}
		else if(!std::strcmp(*arg, "-gdnl")
			 || !std::strcmp(*arg, "--group-delim-line"))
			opts.group_delim = "\n";
		else if(!std::strcmp(*arg, "-gdws")
			 || !std::strcmp(*arg, "--group-delim-space"))
			opts.group_delim = " ";
		else if(!std::strcmp(*arg, "-gdno")
			 || !std::strcmp(*arg, "--group-delim-none"))
			opts.group_delim = "";
		else if(!std::strcmp(*arg, "-gd")
			 || !std::strcmp(*arg, "--group-delim"))
		{
			if(!(*(++arg)))
				throw_runtime_error((*(arg - 1)) << " expects delimiter string argument");
			opts.group_delim = *arg;
		}
		else if(!std::strcmp(*arg, "-gf")
			 || !std::strcmp(*arg, "--group-format"))
		{
			if(!(*(++arg)))
				throw_runtime_error((*(arg - 1)) << " expects string format argument");
			opts.group_format = *arg;
		}
		else if(!std::strcmp(*arg, "-f"))
		{
			if(!(*(++arg)))
				throw_runtime_error("-f expects filename argument");
			opts.input_file = *arg;
		}
		else if(!std::strcmp(*arg, "-l")
			 || !std::strcmp(*arg, "--line"))
			opts.line_by_line = true;
		else if(*arg[0] == '-')
			throw_runtime_error("unrecognized option: " << *arg);
	}

	if(!opts.groups.empty() && opts.group_format)
		throw_runtime_error("Can not have -g# specifiers with -gf format");

	if(opts.groups.empty() && !opts.group_format)
		opts.groups.push_back(0);

	if(debug)
	{
		std::cout << "group_delim : " << opts.group_delim << '\n';
		std::cout << "input_file  : " << opts.input_file << '\n';
		std::cout << "line_by_line: " << opts.line_by_line << '\n';
		std::cout << "line_delim  :" << opts.line_delim << '\n';
		std::cout << "match_delim : " << opts.match_delim << '\n';
		for(auto&& group: opts.groups)
			std::cout << "-g: " << int(group) << '\n';
	}

	return opts;
}

int usage_match(std::ostream& os, usage_mode mode, int error)
{
	(void) mode;
	os << "Match: +m|match" << '\n';
	os << "  Test if the entire input matches the regular expression" << '\n';
	os << "    cppre match [options] [<string>] <regex>" << '\n';
	os << "    cppre    +m [options] [<string>] <regex>" << '\n';
	os << "" << '\n';
	os << "  If the optional <string> parameter is not present the"
		  " match will be against the standard input." << '\n';
	os << "" << '\n';
	return error;
}

int usage_search(std::ostream& os, usage_mode mode, int error)
{
	(void) mode;
	os << "Search: +s|search" << '\n';
	os << "  Test if part of the input matches the regular expression" << '\n';
	os << "    cppre search [options] [<string>] <regex>" << '\n';
	os << "    cppre     +s [options] [<string>] <regex>" << '\n';
	os << "" << '\n';
	os << "  If the optional <string> parameter is not present the"
		  " match will be against the standard input." << '\n';
	return error;
}

int usage_iterate(std::ostream& os, usage_mode mode, int error)
{
	(void) mode;
	os << "Iterate: +i|iterate" << '\n';
	os << "  Find all parts of the input that match the regular expression" << '\n';
	os << "    cppre iterate [options] [<string>] <regex>" << '\n';
	os << "    cppre      +i [options] [<string>] <regex>" << '\n';
	os << "" << '\n';
	os << "  If the optional <string> parameter is not present the"
		  " match will be against the standard input." << '\n';
	return error;
}

int usage_replace(std::ostream& os, usage_mode mode, int error)
{
	(void) mode;
	os << "Replace: +r|replace" << '\n';
	os << "  Replace all parts of the input that match the regular expression" << '\n';
	os << "    cppre replace [options] [<string>] <regex> <replacement>" << '\n';
	os << "    cppre      +r [options] [<string>] <regex> <replacement>" << '\n';
	os << "" << '\n';
	os << "  If the optional <string> parameter is not present the"
		  " match will be against the standard input." << '\n';
	os << "  If the optional <replacement> parameter is an output format"
		  " replacement string that will output the contents of the string"
		  " with $n capture groups replaced by their captured text." << '\n';
	return error;
}

int usage_tokenize(std::ostream& os, usage_mode mode, int error)
{
	(void) mode;
	os << "Tokenize: +t|tokenize" << '\n';
	os << "  Find all parts of the input that match the regular expression" << '\n';
	os << "    cppre iterate [options] [<string>] <regex>" << '\n';
	os << "    cppre      +t [options] [<string>] <regex>" << '\n';
	os << "" << '\n';
	os << "  If the optional <string> parameter is not present the"
		  " match will be against the standard input." << '\n';
	return error;
}

NAMESPACE_CPPRE_END;
