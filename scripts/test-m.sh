#!/bin/bash

CPPRE="./build-release/src/cppre"

${CPPRE} +m -g1 'aabbbccc' '.*(b+).*'
echo
${CPPRE} +m -g1 'aabbbccc' '.*?(b+).*'
echo
